/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/cobra"
)

// mdCmd represents the md command
var mdCmd = &cobra.Command{
	Use:   "md",
	Short: "A brief description of your command",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("md called")
		path, _ := cmd.Flags().GetString("path")
		model, _ := cmd.Flags().GetString("model")
		prop, _ := cmd.Flags().GetString("prop")
		spew.Dump(prop)
		getpath(path, model, prop)
	},
}

func init() {
	rootCmd.AddCommand(mdCmd)
	mdCmd.Flags().StringP("path", "p", "", "Set your path")
	mdCmd.Flags().StringP("model", "m", "", "Set your model")
	mdCmd.Flags().StringP("prop", "r", "", "Set your properties")
}


func getpath(path string, model string, prop string) {
	MF()

	f, err := os.Create(path)
	if err != nil {
		log.Println("create file: ", err)
		return
	}

	type data struct {
		Model string
		Prop  []string
	
	}

var str string
var sd []string
	s := strings.Split(prop, " ")
	for i, ss:= range s{
		str = strings.Replace(ss, ".", " ", 1)
		fmt.Println(i, str)
		sd = append(sd, str)
	}


spew.Dump(s)
	
	var d = data{Model: model, Prop: sd}
	tm := template.Must(template.New("queue").Parse(queueTemplate))
	exec.Command("gofmt")
	exec.Command("goimports")
	err = tm.Execute(f, d)
	if err != nil {
		log.Print("execute: ", err)
		return
	}
	f.Close()
}
//MF makes
func MF() {
	_, err := os.Stat("tests")

	if os.IsNotExist(err) {
		errDir := os.MkdirAll("tests", 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}

var queueTemplate = `
  package queue
  
  import (
	"gorm.io/gorm"
	"gorm.io/driver/sqlite"
  )
  
  type {{.Model}} struct {
	{{range slice .Prop 0 }}
	{{.}} 
	{{end}}
  }

 
  db.AutoMigrate(&{{.Model}}{})

  // Create
  //db.Create(&{{.Model}}{Code: "D42", Price: 100})
  var product {{.Model}}
  db.First(&product, 1)
  `